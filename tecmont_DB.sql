-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 11-Mar-2019 às 00:07
-- Versão do servidor: 5.7.23
-- versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tecmont`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `card`
--

DROP TABLE IF EXISTS `card`;
CREATE TABLE IF NOT EXISTS `card` (
  `id_card` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_card` varchar(50) DEFAULT NULL,
  `img_card` varchar(200) DEFAULT NULL,
  `texto_card` varchar(500) DEFAULT NULL,
  `page` varchar(50) NOT NULL,
  PRIMARY KEY (`id_card`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `card`
--

INSERT INTO `card` (`id_card`, `titulo_card`, `img_card`, `texto_card`, `page`) VALUES
(1, 'PABX', '/cards/pabx.png', '', 'home'),
(2, 'CFTV', '/cards/cftv.png', NULL, 'home'),
(3, 'Condominial', '/cards/cond.png', NULL, 'home'),
(4, 'Acessórios', '/cards/aces.png', '', 'home'),
(5, 'Aparelhos', '/cards/aparelho.png', 'Venda de aparelhos digitais, ip e sem fio.', 'produtos'),
(6, 'Soluções condominiais', '/cards/condominio.png', 'Instalações de interfonia em condominios.', 'produtos'),
(7, 'PABX', '/cards/pabx.png', 'Instalação e configuração de centrais analógicas, digitais, IP e em nuvem', 'produtos'),
(8, 'Segurança', '/cards/seguranca.png', 'Equipamentos de alarme e câmeras.', 'produtos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

DROP TABLE IF EXISTS `contato`;
CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mensagem` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `nome`, `email`, `mensagem`) VALUES
(1, '', '', ''),
(2, 'Renato Tomaz de Oliveira Aquino', 'renato.aquino@etec.sp.gov.br', 'Mensagem teste.'),
(3, 'Renato Tomaz de Oliveira Aquino', 'renato.aquino@etec.sp.gov.br', 'Mensagem Teste 2'),
(4, '', '', ''),
(5, '', '', ''),
(6, '', '', ''),
(7, '', '', ''),
(8, '', '', ''),
(9, '', '', ''),
(10, '', '', ''),
(11, '', '', ''),
(12, '', '', ''),
(13, '', '', ''),
(14, '', '', ''),
(15, '', '', ''),
(16, '', '', ''),
(17, '', '', ''),
(18, '', '', ''),
(19, '', '', ''),
(20, '', '', ''),
(21, '', '', ''),
(22, '', '', ''),
(23, '', '', ''),
(24, '', '', ''),
(25, '', '', ''),
(26, '', '', ''),
(27, '', '', ''),
(28, '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `info`
--

DROP TABLE IF EXISTS `info`;
CREATE TABLE IF NOT EXISTS `info` (
  `id_info` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) NOT NULL,
  `cont` varchar(1000) NOT NULL,
  `page` varchar(50) NOT NULL,
  PRIMARY KEY (`id_info`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `info`
--

INSERT INTO `info` (`id_info`, `titulo`, `cont`, `page`) VALUES
(1, 'Quem somos', 'A Tecmont atua no mercado de telecomunicações desde 1988, sempre oferecendo produtos e serviços de alta qualidade aos clientes.\r\n \r\nHá mais de 25 anos se especializa no mercado de telecomunicações, possui uma equipe própria e altamente qualificada.\r\n \r\nTrabalhamos com venda, locação e manutenção de centrais telefônicas, interfones, CFTV, No-breaks, atendedores automáticos, tarifadores para controle de custos, interfaces de celular, redes estruturadas, etc.', 'empresa'),
(2, 'Missão', 'Buscar, por meio de parcerias com fabricantes e distribuidores, soluções de alta tecnologia e inovações para oferecer aos clientes. Proporcionando, assim, que possam alcançar os benefícios e soluções que eles procuram.', 'empresa'),
(3, 'Visão', 'Estar sempre entre as melhores empresas de telecomunicações, reconhecida, por todos os clientes, como uma empresa diferenciada pela qualidade dos serviços prestados.', 'empresa'),
(4, 'Serviços e soluções', 'Atuamos seguindo as normas técnicas e orientações dos fabricantes, sempre buscando manter os equipamentos em perfeito estado de funcionamento. Por meio de contrato de manutenção com visitas preventivas e corretivas ou atendimentos avulsos.<br>\r\nOferecemos aos nossos clientes os seguintes serviços:<br><br>\r\n \r\n- Instalação e manutenção de centrais telefônicas;<br><br>\r\n \r\n- Instalação e manutenção de CFTV;<br><br>\r\n \r\n- Lançamento, conectorização e montagem de rede estruturada;<br><br>\r\n \r\n- Instalação de rede interna de ramais;<br><br>\r\n \r\n- Instalação de rede interna predial;<br><br>\r\n \r\n- Gravação de esperas e atendedores personalizados;<br><br>\r\n \r\n- Integração da rede telefônica fixa e móvel para redução de custos;<br><br>\r\n \r\n-Reparos em placas e aparelhos telefônicos;<br><br>\r\n \r\n- Venda dos serviços de voz e banda larga da empresa Vivo, intermediando a negociação desde a contratação até a implantação.', 'servicos');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
