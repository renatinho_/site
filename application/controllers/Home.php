<?php

    defined('BASEPATH') OR exit('No direct script access allowed'); 

    class Home extends CI_Controller{
        //Controler Principal do projeto


        public function index(){//Pagina inicial
            $this->load->view('common/header');
            $this->load->view('common/navbar');//Chama o codigo da navbar

            $this->load->view('home/slide');
            $this->load->model('CardModel');
            $this->CardModel->card_show('home');//Exibe os cards passando para a função qual a pagina atual

            $this->load->view('common/footer_code');//Chama o codigo do footer    
            $this->load->view('common/footer');


        }

        public function empresa()
        {
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('InfoModel');
            $data['info'] = $this->InfoModel->info_data('empresa'); //Recebe oque esta no banco sobre a paguina 
            $this->load->view('info_view', $data);//Exibe todas as informações que estão no banco sobre a pagina 'empresa'


            $this->load->view('common/footer_code');
            $this->load->view('common/footer');
        }

        public function contato()
        {
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('ContatoModel', 'model');
            $this->model->enviar();//Envia para o banco o que foi preenchido no formulario de contato
            $this->load->view('contact_form');

            $this->load->view('common/footer_code');
            $this->load->view('common/footer');
        }

        public function servicos()
        {
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('InfoModel');
            $data['info'] = $this->InfoModel->info_data('servicos'); //Recebe oque esta no banco sobre a paguina 
            $this->load->view('info_view', $data);//Exibe todas as informações que estão no banco sobre a pagina 'servicos'


            $this->load->view('common/footer_code');
            $this->load->view('common/footer');
        }

        public function produtos()
        {
            $this->load->view('common/header');
            $this->load->view('common/navbar');


            $this->load->view('row');
            $this->load->model('CardModel');
            $this->CardModel->card_show('produtos');//Exibe os cards passando para a função qual a pagina atual

            $this->load->view('common/footer_code');
            $this->load->view('common/footer');
        }

        public function alterar()
        {
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('CardModel');
            $v['lista'] = $this->CardModel->lista();//Recebe uma lista com todos os cards e armazena em v
            $this->load->view('table_view', $v);//Exibe a lista
            $this->CardModel->atualizar();//Atualiza um card

            $this->CardModel->criar();//Cria um card
            $this->load->view('add_card');//Botao para inserir o card

            $this->load->model('InfoModel');
            $v['lista'] = $this->InfoModel->lista();//Recebe uma lista com todas as informações e armazena em v
            $this->load->view('table_view', $v);//Exibe a lista
            $this->InfoModel->atualizar(); //Atualiza uma informação
            
            $this->InfoModel->criar();//Cria uma informação
            $this->load->view('add_info');//Botao para inserir o card



            $this->load->view('common/footer_code');
            $this->load->view('common/footer');
        }

        public function delete($id, $model)//pagina chamada na função delete que a deleta e redireciona para 'atualizar'
        {
            $this->load->model($model, 'model');
            $this->model->delete($id);
            redirect('home/alterar/');
        }
    }


?>