<?php

include_once APPPATH.'libraries/Info.php';
     class InfoModel extends CI_Model{
        public function info_data($page)
        {
            $query = $this->db->get_where('info', array('page' => $page));
            return $query->result_array();

        }
        
        public function lista()
        {
            $html = '';
            $info = new Info();
            // organiza a lista e depois retorna o resultado
            $data = $info->getAll();

            $html .= '<h2>Paginas de informações</h2>';
            $html .= '<table class="table">';
            $html .=  '<thead>';
            $html .= '<tr>';
            $html .= '<th scope="col md-2">Titulo</td>';
            $html .= '<th scope="col md-6">Conteudo</td>';
            $html .= '<th scope="col md-2">Pagina de exibição</td>';
            $html .= '<th scope="col md-2">Ações</td></tr>';
            $html .=  '</thead>';

            $html .=  '<tbody>';
            foreach ($data as $row) {
                $html .= '<tr>';
                $html .= '<td>'.$row['titulo'].'</td>';
                $html .= '<td>'.$row['cont'].'</td>';
                $html .= '<td>'.$row['page'].'</td>';
                $html .= '<td>'.$this->get_edit_icons($row['id_info']).'</td></tr>';
            }
            $html .=  '</tbody>';
            $html .= '</table>';
            return $html;
        }

        private function get_edit_icons($id){
            $html = $this->edit_span($id);
            $html .= '<a data-toggle="modal" data-target="#info'.$id.'""><i class="far fa-edit mr-3 text-primary"></i></a>';
        
            $html .= '<a href="'.base_url('home/delete/'.$id.'/InfoModel').'"><i class="far fa-trash-alt text-danger"></i></a>';
            return $html;
        }
        

        private function edit_span($id){

            $info = new Info();

            $data = $info->getById($id);

            $html = '<div class="modal fade" id="info'.$id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header text-center">
                  <h4 class="modal-title w-100 font-weight-bold">Editar informação</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="POST">
                <div class="modal-body mx-3">
                  <div class="md-form mb-5">
                    <input type="hidden" name="id_info" value='.$data['id_info'].'>
                    <label data-error="wrong" data-success="right" >Titulo</label>
                    <input type="text" id="titlulo"  name="titulo" class="form-control validate" value="'.$data['titulo'].'">                    
                  </div>
          
                  <div class="md-form mb-5">
                    <input type="text" id="page" class="form-control validate" name="page" value="'.$data['page'].'">
                    <label data-error="wrong" data-success="right" for="page">Pagina de exibição</label>
                  </div>
          
                  <div class="md-form">
                    <label>Conteúdo</label>
                    <textarea type="text" id="cont" class="form-control rounded-0 mt-4" name="cont" rows="4">'.$data['cont'].'</textarea>
                    
                  </div>
          
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-info my-4 btn-block" type="submit">Enviar</button>
                </div>
              </div>
              </form>
            </div>
          </div>
          

            ';
            return $html;
        }
        public function atualizar()
        {
            if(sizeof($_POST) == 0) return;
            $data = $this->input->post();
            if(isset($data['id_info']) && $data['id_info'] != 'cria'){             
                $info = new Info();
                if($info->update($data, $data['id_info'])){
                    redirect('home');
                }
            }
            

        }


        public function criar()
        {
          if(sizeof($_POST) == 0) return;
          // $data = $this->input->post();
          // print_r($data);
          if($this->input->post('id_info') == 'cria'){
            $titulo = $this->input->post('titulo');
            $cont = $this->input->post('cont');
            $page = $this->input->post('page');

            $info = new Info($titulo, $cont, $page);
            $info->save();
          }
		}
		
		public function delete($id)
		{
			$info = new Info();
			$info->delete($id);
		}
		

     }



?>