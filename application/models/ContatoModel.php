<?php

    include_once APPPATH.'libraries/Contato.php';

    class ContatoModel extends CI_Model
    {
        //Recebe o preenchimento do formulario e as envia para a Classe Contato
        public function enviar()
        {
            if(isset($_POST) == 0) return;

            $nome = $this->input->post('nome');
            $email = $this->input->post('email');
            $mensagem = $this->input->post('mensagem');

            $contato = new Contato($nome , $email, $mensagem);
            $contato->save();
        }
    }
    

?>