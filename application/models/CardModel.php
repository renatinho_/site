<?php

    include_once APPPATH.'libraries/Card.php';

     class CardModel extends CI_Model{

        private $id_aux;

        
        //Exibe os cards de uma determinada tela
        public function card_show($page){

            $card = new Card();
            $data = $card->card_data($page);

            for($i = 0; $i < sizeof($data); $i++){
                $this->load->view('common/card', $data[$i]);
            }
        }

        //Lista com todos os cards
        public function lista()
        {
            $html = '';
            $card = new Card();
            // organiza a lista e depois retorna o resultado
            $data = $card->getAll();

            $html .= '<h2>Cards</h2>';
            $html .= '<table class="table">';
            $html .=  '<thead>';
            $html .= '<tr>';
            $html .= '<th scope="col">Titulo</td>';
            $html .= '<th scope="col">Imagem</td>';
            $html .= '<th scope="col">Conteudo</td>';
            $html .= '<th scope="col">Pagina de exibição</td>';
            $html .= '<th scope="col">Ações</td></tr>';
            $html .=  '</thead>';

            $html .=  '<tbody>';
            foreach ($data as $row) {
                $html .= '<tr>';
                $html .= '<td>'.$row['titulo_card'].'</td>';
                $html .= '<td>'.$row['img_card'].'</td>';
                $html .= '<td>'.$row['texto_card'].'</td>';
                $html .= '<td>'.$row['page'].'</td>';
                $html .= '<td>'.$this->get_edit_icons($row['id_card']).'</td></tr>';
            }
            $html .=  '</tbody>';
            $html .= '</table>';
            return $html;
        }

        //Faz os icones de alteração e exclusão da lista com o id correspondente
        private function get_edit_icons($id){
            $html = $this->card_span($id);
            $html .= '<a data-toggle="modal" data-target="#Card'.$id.'""><i class="far fa-edit mr-3 text-primary"></i></a>';
            $html .= '<a href="'.base_url('home/delete/'.$id.'/CardModel').'"><i class="far fa-trash-alt text-danger"></i></a>';
            return $html;
        }

        //Span do formulario de alteração
        public function card_span($id)
        {
            $card = new Card();

            $data = $card->getById($id);

            $html = '<div class="modal fade" id="Card'.$id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header text-center">
                  <h4 class="modal-title w-100 font-weight-bold">Editar informação</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="POST">
                <div class="modal-body mx-3">
                  <div class="md-form mb-5">
                  <input type="hidden" name="id_card" value='.$data['id_card'].'>
                    <label data-error="wrong" data-success="right" >Titulo</label>
                    <input type="text" id="titlulo" name="titulo_card" class="form-control validate" value="'.$data['titulo_card'].'">                    
                  </div>

                  <div class="md-form mb-5">
                    <label data-error="wrong" data-success="right" >Imagem</label>
                    <input type="text" id="titlulo" name="img_card" class="form-control validate" value="'.$data['img_card'].'">                    
                  </div>
          
                  <div class="md-form mb-5">
                    <input type="text" id="page" name="page" class="form-control validate" value="'.$data['page'].'">
                    <label data-error="wrong" data-success="right" for="page">Pagina de exibição</label>
                  </div>
          
                  <div class="md-form">
                    <label>Conteúdo</label>
                    <textarea type="text" id="cont" name="texto_card" class="form-control rounded-0 mt-4" rows="4">'.$data['texto_card'].'</textarea>
                    
                  </div>
          
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-info my-4 btn-block" type="submit">Enviar</button>
                </div>

              </div>
              </form>
            </div>
          </div>
          

            ';
            return $html;
        }


        //Recebe as informações a serem alteradas e as envia para a classe Card
        public function atualizar()
        {
            if(sizeof($_POST) == 0) return;
            $data = $this->input->post();
            if(isset($data['id_card']) && $data['id_card'] != 'cria'){
            $card = new Card();
            if($card->update($data, $data['id_card'])){
                redirect('home');
            }
            }
            

        }


        //Recebe as informações e as envia para a classe Card
        public function criar()
        {
          if(sizeof($_POST) == 0) return;
          // $data = $this->input->post();
          // print_r($data);
          if($this->input->post('id_card') == 'cria'){
            $titulo = $this->input->post('titulo_card');
            $img = $this->input->post('img_card');
            $page = $this->input->post('page');
            $card = new Card($titulo, $img, $page);
            if(null != $this->input->post('texto_card')){
              $card->setTexto($this->input->post('texto_card'));
            }
            else{
              $card->setTexto(null);
            }
            $card->save();
          }
        }

        //Recebe as informações a serem deletadas e as envia para a classe Card
        public function delete($id)
        {
          $card = new Card();
          $card->delete($id);
        }


     }



?>