<div class="modal fade" id="Card" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header text-center">
                  <h4 class="modal-title w-100 font-weight-bold">Adicionar Card</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="POST">
                <div class="modal-body mx-3">
                  <div class="md-form mb-5">
                  <input type="hidden" name="id_card" value='cria'>
                    <label data-error="wrong" data-success="right" >Titulo</label>
                    <input type="text" id="titlulo" name="titulo_card" class="form-control validate">                    
                  </div>

                  <div class="md-form mb-5">
                    <label data-error="wrong" data-success="right" >Imagem</label>
                    <input type="text" id="titlulo" name="img_card" class="form-control validate">                    
                  </div>
          
                  <div class="md-form mb-5">
                    <input type="text" id="page" name="page" class="form-control validate">
                    <label data-error="wrong" data-success="right" for="page">Pagina de exibição</label>
                  </div>
          
                  <div class="md-form">
                    <label>Conteúdo</label>
                    <textarea type="text" id="cont" name="texto_card" class="form-control rounded-0 mt-4" rows="4"></textarea>
                    
                  </div>
          
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-info my-4 btn-block" type="submit">Enviar</button>
                </div>

              </div>
              </form>
            </div>
          </div>
<div class="row mb-5">
    <div class="col">
        <a data-toggle="modal" data-target="#Card"><i class="fas fa-plus"></i> Adicionar</a>    
    </div>
</div>
