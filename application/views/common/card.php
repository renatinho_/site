        <!-- Card -->
<div class="col-sm">
        <div class="card">

        <!-- Card image -->
        <div class="view overlay">
            <img class="card-img-top" src="<?= base_url('assets/img'.$img_card)?>" alt="Card image cap">
            <a href="#!">
            <div class="mask rgba-white-slight"></div>
            </a>
        </div>

        <!-- Card content -->
        <div class="card-body">

            <!-- Title -->
            <h4 class="card-title"><?= $titulo_card ?></h4>

            <!-- Text -->
            <?php
                if (isset($texto_card)) {
                    echo "<p class='card-text'> $texto_card </p>";
                }
            
            ?>
            <!-- Button -->
            <a href="#" class="btn btn-primary">Ver mais</a>

        </div>

        </div>
        <!-- Card -->
        </div>