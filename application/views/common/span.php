<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Editar informação</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body mx-3">
        <div class="md-form mb-5">
        <input type="text" id="titlulo" class="form-control validate">
        <label data-error="wrong" data-success="right" for="titulo">Titulo</label>
        </div>

        <div class="md-form mb-5">
        <input type="text" id="page" class="form-control validate">
        <label data-error="wrong" data-success="right" for="page">Pagina de exibição</label>
        </div>

        <div class="md-form">
        <textarea type="text" id="cont" class="md-textarea form-control" rows="4"></textarea>
        <label data-error="wrong" data-success="right" for="cont">Conteúdo</label>
        </div>

    </div>
    <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-unique">Send <i class="fas fa-paper-plane-o ml-1"></i></button>
    </div>
    </div>
</div>
</div>