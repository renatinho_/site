</div>
</div>
<!-- Footer -->
<footer class="page-footer font-small mdb-color info-color-dark pt-4 mt-5">

  <!-- Footer Elements -->
  <div class="container">

    <!--Grid row-->
    <div class="row">

      <!--Grid column-->
      <div class="col-lg-2 col-md-12 mb-4 ml-5">

        <!--Image-->
        <div class="view">
          <img src="<?= base_url('assets/img/parceiros/panasonic.png') ?>" class="img-fluid w-50" alt="">
        </div>

      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-2 col-md-6 mb-4 ml-3">

        <!--Image-->
        <div class="view">
          <img src="<?= base_url('assets/img/parceiros/intelbras.png') ?>" class="img-fluid" alt="">
        </div>

      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-2 col-md-6 mb-4 ml-3">

        <!--Image-->
        <div class="view">
          <img src="<?= base_url('assets/img/parceiros/exotec.png') ?>" class="img-fluid" alt="">
        </div>

      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-2 col-md-12 mb-4 ml-3">

        <!--Image-->
        <div class="view">
          <img src="<?= base_url('assets/img/parceiros/urmet.png') ?>" class="img-fluid" alt="">
        </div>

      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-2 col-md-6 mb-4 ml-3">

        <!--Image-->
        <div class="view">
          <img src="<?= base_url('assets/img/parceiros/vivo.png') ?>" class="img-fluid" alt="">
        </div>

      </div>
      <!--Grid column-->

    </div>
    <!--Grid row-->

  </div>
  <!-- Footer Elements -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2018 Copyright:
    <a href="https://mdbootstrap.com/education/bootstrap/"> TECMONT TELECOMUNICAÇÕES</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
