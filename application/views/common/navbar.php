<nav class="navbar navbar-expand-lg navbar-dark blue-gradient indigo">
    <a class="navbar-brand font-weight-bold h3-responsive" href="<?= base_url('home')?>">Tecmont</a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('home/empresa/')?>">Empresa
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('home/servicos/')?>">Serviços</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('home/produtos/')?>">Produtos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('home/contato/')?>">Contato</a>
            </li>
            
            <li class="nav-item float-left">
                <a class="nav-link" href="<?= base_url('home/alterar/')?>">Alterar Pagina
                    <span class="sr-only">(current)</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">