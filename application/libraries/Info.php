<?php
    //Classe que manipula as o conteudo das paginas informativas do site
    class Info{
        // Obrigatorio
        private $titulo;
        private $cont;
        private $page;//Determina a pagina que a informação aparecera que ja esta preveamente guardada no banco

        private $db;

        public function __construct($titulo = null, $cont = null, $page = null) 
        {
            $this->titulo = $titulo;
            $this->cont = $cont;
            $this->page = $page;

            $ci = &get_instance();
            $this->db = $ci->db;
        }


        //Cria uma nova informação no banco
        public function save()
        {
            $sql = "INSERT INTO info (titulo, cont, page) VALUES ('$this->titulo', '$this->cont', '$this->page')";
            $this->db->query($sql);
            redirect('home/alterar/');
        }


        //recebe todos os dados do banco e os retorna como array
        public function getAll()
        {
            $sql = "SELECT * FROM info";
            $res = $this->db->query($sql);
            return $res->result_array();
        }


        //Retorna do banco um dado especifico pelo id
        public function getById($id)
        {
            $rs = $this->db->get_where('info', "id_info = $id");
            return $rs->row_array();
        }


        //Atualiza o conteudo de uma informação pelo id
        public function update($data, $id)
        {
            $this->db->update('info', $data, "id_info = $id");
            return $this->db->affected_rows();
        }

        //Deleta uma informação do banco
        public function delete($id)
        {
            $this->db->delete('info', "id_info = $id");
        }

    }


?>