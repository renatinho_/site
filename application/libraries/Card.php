<?php

    //Classe que recebe e envia as informações sobre os cards do site
    class Card{
        // Obrigatorio
        private $titulo;
        private $img;
        private $page;//Determina a pagina que o aparecera que ja esta preveamente guardada no banco

        
        //Opcional
        private $texto = null;

        private $db;

        //Construct da classe
        public function __construct($titulo = null, $img = null, $page = null) 
        {
            $this->titulo = $titulo;
            $this->img = $img;
            $this->page = $page;

            $ci = &get_instance();
            $this->db = $ci->db;
        }

        //Setando o valor texto por ele não ser obrigatório
        public function setTexto($cont){
            $this->texto = $cont;
        }

        //função que insere no banco as informações de um novo card
        public function save()
        {
            $sql = "INSERT INTO card (titulo_card, img_card, texto_card, page) VALUES ('$this->titulo', '$this->img', '$this->texto','$this->page')";
            $this->db->query($sql);
            redirect('home/alterar/');
        }


        //Puxa todas as informações do banco e as retorna como um array
        public function getAll()
        {
            $sql = "SELECT * FROM card";
            $res = $this->db->query($sql);
            return $res->result_array();
        }

        //Puxa as informações do banco pelo seu id
        public function getById($id)
        {
            $rs = $this->db->get_where('card', "id_card = $id");
            return $rs->row_array();
        }


        //Recebe as informações e as altera no banco pelo id
        public function update($data, $id)
        {
            $this->db->update('card', $data, "id_card = $id");
            return $this->db->affected_rows();
        }

        //Deleta um card pelo id
        public function delete($id)
        {
            $this->db->delete('card', "id_card = $id");
        }

        //Recebe os card de uma pagina especifica
        public function card_data($page)
        {
            $query = $this->db->get_where('card',array('page' => $page));
            return $query->result_array();
        }

    }


?>